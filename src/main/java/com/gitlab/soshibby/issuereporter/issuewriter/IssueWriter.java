package com.gitlab.soshibby.issuereporter.issuewriter;

public interface IssueWriter {
    void init(String config);
    void writeIssue(String hash, String header, String body);
    void writeIncident(String hash, String body);
}
